import requests
from bs4 import BeautifulSoup
import csv


# Получаем html-код страницы
def get_html(url):
    r = requests.get(url)
    return r.text


# Получаем кол-во страниц с интересующей информацией
def get_total_pages(html):
    soup = BeautifulSoup(html, 'lxml')
    pages = soup.find('div', class_='pagination-pages').find_all('a', class_='pagination-page')[-1].get('href')
    total_pages = pages.split('=')[1].split('&')[0]
    return int(total_pages)


# Запись в csv-файл
def write_csv(data):
    with open('avito.csv', 'a', encoding='utf-8') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow((
            data['title'],
            data['price'],
            data['metro'],
            data['url']
        ))


# Получаем следующие даные о продукции Apple из объявлений: заголовок, url, цена, метро
def get_page_data(domen, html):
    soup = BeautifulSoup(html, 'lxml')
    ads = soup.find('div', class_='catalog-list').find_all('div', class_='item_table')
    for ad in ads:
        name = ad.find('div', class_='description').find('h3').text.strip().lower()

        if 'apple' in name:
            try:
                title = ad.find('div', class_='description').find('h3').text.strip()
            except:
                title = ''

            try:
                url = domen + ad.find('div', class_='description').find('h3').find('a').get('href')
            except:
                url = ''

            try:
                price = ad.find('div', class_='about').text.strip()
            except:
                price = ''

            try:
                metro = ad.find('div', class_='data').find_all('p')[-1].text.strip()
            except:
                metro = ''

            data = {
                'title': title,
                'price': price,
                'metro': metro,
                'url': url
            }

            write_csv(data)


def main():
    print('Начало работы скрипта')
    domen = 'https://www.avito.ru'
    url = 'https://www.avito.ru/kazan/telefony?p=1&q=Apple'
    base_url = 'https://www.avito.ru/kazan/telefony?'
    page_part = 'p='
    query_part = '&q=Apple'
    total_pages = get_total_pages(get_html(url))
    for i in range(1, total_pages+1):
        print('Работа с страницей №' + str(i))
        url_gen = base_url + page_part + str(i) + query_part
        html = get_html(url_gen)
        get_page_data(domen, html)
    print('Завершение работы скрипта')


if __name__ == '__main__':
    main()